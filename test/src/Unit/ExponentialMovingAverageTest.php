<?php

namespace Drupal\Tests\queue_stats\Unit;

use Drupal\queue_stats\QueueStatistics;
use Drupal\queue_stats\Statistics\ExponentialMovingAverage;
use Drupal\Tests\UnitTestCase;

/**
 * Unit test for calculating exponential moving average.
 */
class ExponentialMovingAverageTest extends UnitTestCase {

  /**
   * Test combinations of factor and data.
   *
   * This essentially tests the exponentially moving average calculations.
   *
   * @dataProvider processingTimesFactors
   */
  public function testVaryingValueFactors(int $factor, array $values, $expected, $reason) {
    $average = new ExponentialMovingAverage($factor);
    foreach ($values as $value) {
      $average->add($value);
    }
    $this->assertEquals($expected, $average->average(), $reason);
  }

  /**
   * Data provider for testVaryingProcessingTimesFactors().
   *
   * @return array
   *   Data sets.
   */
  public function processingTimesFactors() {
    return [
      [
        1, [1, 2], 2,
        'When using factor 1 the moving average should disregard previous data'
      ],
      [
        2, [1, 2], 1.5,
        'When using factor 2 the moving average should be halfway between the
        two first data points'
      ],
      [
        3, [1, 2], 1.5,
        'When using a factor higher than the number of provided data points (3)
        then the number of data points (2) will be used as the factor.'
      ],
      [
        2, [1, 2, 1], 1.25,
        'When using a factor 2 with 3 data points then the average will be
        halfway between the average of the first two data points and the third
        data point.'
      ]
    ];
  }

}
