INTRODUCTION
------------

Queues are very useful when wanting to offload tasks to background processes.
However it can be difficult to get an overview of how they are running. How
long does it take before an item is processed? Are the resources allocated to
processing items sufficient compared to how many items are generated?

Queue statistics tries to solve this problem by collecting and displaying
statistics about queues used on a Drupal site.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.


If you also use the Queue UI module then a version including the following
commit is recommended. This will ensure that queue inspection functionality
provided by Queue UI still works:
https://cgit.drupalcode.org/queue_ui/commit/?id=20057d6


USAGE
-----

The module requires no configuration and will collect statistics from all queues
by default.

All collected statistics are shown on module configuration page available from
the Extend list. This page also allows you to reset the statistics. If you use
the Queue UI module this is also available as a task under the Queue Manager
page.

The module also provides a Drupal block which allows you to display a table of
selected queues and statistics wherever blocks are supported.


EXTENSION
---------

Individual queue statistics are implemented as Drupal plugins and you can extend
the module by implementing your own plugins.

As an example here is one of the plugins provided by this module:

```
<?php

namespace Drupal\queue_stats\Plugin\QueueStatistic;

use Drupal\queue_stats\Annotation\QueueStatistic;
use Drupal\queue_stats\MonitoredQueueInterface;
use Drupal\queue_stats\Plugin\QueueStatisticBase;

/**
 * Determine how many items are in a queue currently.
 *
 * @QueueStatistic(
 *   id = "num_items",
 *   label = "Number of items",
 *   order = -100
 * )
 */
class NumItems extends QueueStatisticBase {

  /**
   * {@inheritdoc}
   */
  public function getValue(MonitoredQueueInterface $queue) {
    return $queue->numberOfItems();
  }

}
```

More examples including how to base statistics on processing of individual items
can be found in `src/Plugin/QueueStatistic`.


DESIGN DECISIONS
----------------

Plugins can react to queue items being processed by subscribing to instances of
`QueueItemEvent`. These are emitted by a decorator around the original queue
implementation.

All current plugins calculate values based on exponential weighted moving
averages. This way each plugin only has to store a single object per queue
instead of having to store individual samples. See
https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average for more
information. The module uses a smoothing factor of 10.

Other plugins are free to use a different method for storing data and
calculating statistical values.
