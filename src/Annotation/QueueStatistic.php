<?php

namespace Drupal\queue_stats\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Queue statistic item annotation object.
 *
 * @see \Drupal\queue_stats\Plugin\QueueStatisticManager
 * @see plugin_api
 *
 * @Annotation
 */
class QueueStatistic extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The order in which to display the statistic.
   *
   * Lower numbers are displayed first. Optional. Defaults to 0.
   *
   * @var int
   */
  public $order;

}
