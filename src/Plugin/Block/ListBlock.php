<?php

namespace Drupal\queue_stats\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\queue_stats\Plugin\QueueStatisticInterface;
use Drupal\queue_stats\Plugin\QueueStatisticManager;
use Drupal\queue_stats\QueueStatisticsCollectorFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block showing a list of queues with their stats.
 *
 * @Block(
 *  id = "queue_stats_list_block",
 *  admin_label = @Translation("Queue statistics list"),
 * )
 */
class ListBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue worker factory.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  protected $queueStatManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new ListBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   The queue worker manager factory.
   * @param \Drupal\queue_stats\Plugin\QueueStatisticManager $queue_stat_manager
   *   The queue stats manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    QueueFactory $queue_factory,
    QueueWorkerManagerInterface $queue_worker_manager,
    QueueStatisticManager $queue_stat_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queueFactory = $queue_factory;
    $this->queueWorkerManager = $queue_worker_manager;
    $this->queueStatManager = $queue_stat_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('plugin.manager.queue_stat')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['queues' => [], 'statistics' => [], 'hide_if_empty' => FALSE] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $queue_options = array_map(function (array $queue_definition) {
      return (string) $queue_definition['title'];
    }, $this->queueWorkerManager->getDefinitions());

    $stat_options = array_map(function (array $stat_definition) {
      return (string) $stat_definition['label'];
    }, $this->queueStatManager->getDefinitions());

    $form['queues'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Queues to show'),
      '#description' => $this->t('If none are selected all queues will be shown.'),
      '#options' => $queue_options,
      '#default_value' => $this->configuration['queues'] ?? [],
    ];

    $form['statistics'] = [
      '#type' => 'checkboxes',
      '#title' => 'Statistics to show',
      '#description' => $this->t('If none are selected all statistics will be shown for each queue.'),
      '#options' => $stat_options,
      '#default_value' => $this->configuration['statistics'] ?? [],
    ];

    $form['hide_if_empty'] = [
      '#type' => 'select',
      '#title' => $this->t('Hide queue if statistic is empty'),
      '#options' => $stat_options,
      '#default_value' => $this->configuration['hide_if_empty'],
      '#empty_value' => FALSE,
      '#description' => $this->t('If the selected statistic is empty (e.g. not defined or 0) for a queue then the queue will not be shown in the list. If no queues are to be shown then the entire block will be hidden.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['queues'] = array_filter($form_state->getValue('queues'));
    $this->configuration['statistics'] = array_filter($form_state->getValue('statistics'));
    $this->configuration['hide_if_empty'] = $form_state->getValue('hide_if_empty');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    // Do not cache statistics. We cannot control when to invalidate them.
    $build['#cache'] = ['max-age' => 0];

    $selected_queues = $this->queueWorkerManager->getDefinitions();
    if (!empty(array_filter($this->configuration['queues']))) {
      $selected_queues = array_intersect_key($selected_queues, array_flip($this->configuration['queues']));
    }

    $selected_stats = $this->queueStatManager->getDefinitions();
    if (!empty(array_filter($this->configuration['statistics']))) {
      $selected_stats = array_intersect_key($selected_stats, array_flip($this->configuration['statistics']));
    }

    $stats = array_map(function (array $stat_definition) {
      return $this->queueStatManager->createInstance($stat_definition['id']);
    }, $selected_stats);

    $hide_stat_id = $this->configuration['hide_if_empty'];
    $hide_stat = $stats[$hide_stat_id] ?? NULL;

    // Collect statistics as render arrays for rows in a table.
    $queue_stats = array_filter(array_map(function (array $queue_definition) use ($stats) {
      $queue = $this->queueFactory->get($queue_definition['id']);

      // Do not render row if controlling statistic is set and empty.
      if (!empty($hide_stat) && empty($hide_stat->getValue($queue))) {
        return;
      };

      $queue_stats = [
        'title' => [
          '#markup' => (string) $queue_definition['title'],
        ]
      ];

      $queue_stats += array_map(function (QueueStatisticInterface $statistic) use ($queue) {
        return [
          '#markup' => $statistic->formatValue($statistic->getValue($queue))
        ];
      }, $stats);

      return $queue_stats;
    }, $selected_queues));

    // If we have some stats to display then render the table.
    if (!empty($queue_stats)) {
      $build['queues'] = [
        '#type' => 'table',
        '#header' => [
          'title' => t('Title'),
        ]
      ];
      $stat_titles = array_map(function (array $stat_definition) {
        return (string) $stat_definition['label'];
      }, $selected_stats);
      $build['queues']['#header'] += $stat_titles;

      $build['queues'] += $queue_stats;
    }

    return $build;
  }

}
