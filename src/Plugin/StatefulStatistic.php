<?php

namespace Drupal\queue_stats\Plugin;

use Drupal\queue_stats\MonitoredQueueInterface;

/**
 * Trait for statistics which store their state between events.
 */
trait StatefulStatistic {

  /**
   * The state interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Return a parent key for the queue under which statistics should store data.
   *
   * @param \Drupal\queue_stats\MonitoredQueueInterface $queue
   *   The queue.
   *
   * @return string
   *   The parent key name.
   */
  protected function stateKey(MonitoredQueueInterface $queue) {
    return "queue_statistics.{$queue->getName()}.{$this->getPluginId()}";
  }

  /**
   * Store a value for a queue.
   *
   * @param \Drupal\queue_stats\MonitoredQueueInterface $queue
   *   The queue to store a value for.
   * @param string $name
   *   The name of the value to store.
   * @param mixed $data
   *   The date to store.
   */
  protected function storeValue(MonitoredQueueInterface $queue, string $name, $data) {
    $this->state->set("{$this->stateKey($queue)}.{$name}", $data);
  }

  /**
   * Retrieve a value for queue.
   *
   * @param \Drupal\queue_stats\MonitoredQueueInterface $queue
   *   The queue to retrieve a value for.
   * @param string $name
   *   The name of the value to store.
   * @param mixed $default
   *   The default value to retrieve if no value is stored.
   *
   * @return mixed
   *   The retrieved value.
   */
  protected function retrieveValue(MonitoredQueueInterface $queue, string $name, $default = 0) {
    return $this->state->get("{$this->stateKey($queue)}.{$name}", $default);
  }

  /**
   * Delete a stored entry.
   *
   * @param \Drupal\queue_stats\MonitoredQueueInterface $queue
   *   The queue to delete a value for.
   * @param string $name
   *   The name of the entry to store.
   */
  public function deleteValue(MonitoredQueueInterface $queue, string $name) {
    $this->state->delete("{$this->stateKey($queue)}.{$name}");
  }

}
