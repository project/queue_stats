<?php

namespace Drupal\queue_stats\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\queue_stats\MonitoredQueueInterface;

/**
 * Defines an interface for Queue statistic plugins.
 */
interface QueueStatisticInterface extends PluginInspectionInterface {

  /**
   * Returns a short description for the statistic.
   *
   * @return string
   *   Statistic label.
   */
  public function getLabel();

  /**
   * Return the statistics value for a queue.
   *
   * @param \Drupal\queue_stats\MonitoredQueueInterface $queue
   *   The queue to show statistics for.
   *
   * @return int|float
   *   Statistic value.
   */
  public function getValue(MonitoredQueueInterface $queue);

  /**
   * Format the value returned from getValue() for display.
   *
   * @param int|float $value
   *   The value to format.
   *
   * @return string
   *   Formatted value.
   */
  public function formatValue($value);

  /**
   * Reset the statistic for a queue.
   *
   * @param \Drupal\queue_stats\MonitoredQueueInterface $queue
   *   The queue to reset statistics for.
   */
  public function reset(MonitoredQueueInterface $queue);

}
