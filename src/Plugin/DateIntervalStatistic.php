<?php

namespace Drupal\queue_stats\Plugin;

/**
 * Trait for statistics in the form of date intervals.
 */
trait DateIntervalStatistic {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Format a value for display.
   *
   * @param int|float $value
   *   The value to display.
   *
   * @return string
   *   The formatted value.
   */
  public function formatValue($value) {
    if (!empty($value)) {
      return $this->dateFormatter->formatInterval($value, 1);
    }
    else {
      return '';
    }
  }

}
