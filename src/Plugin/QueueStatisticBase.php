<?php

namespace Drupal\queue_stats\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\queue_stats\MonitoredQueueInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Queue statistic plugins.
 */
abstract class QueueStatisticBase extends PluginBase implements QueueStatisticInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    $definition = $this->getPluginDefinition();
    return $definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function formatValue($value) {
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function reset(MonitoredQueueInterface $queue) {
    // Do nothing. This plugin does not actually store anything yet.
  }

}
