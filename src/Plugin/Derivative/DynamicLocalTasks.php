<?php

namespace Drupal\queue_stats\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Defines dynamic local tasks.
 */
class DynamicLocalTasks extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Add local tasks for the Queue Manager page if Queue UI is enabled.
    if (\Drupal::moduleHandler()->moduleExists('queue_ui')) {
      $this->derivatives['queue_stats.list'] = array_merge(
        $base_plugin_definition,
        [
          'title' => 'List',
          'route_name' => 'queue_ui.overview_form',
          'base_route' => 'queue_ui.overview_form',
        ]
      );
      $this->derivatives['queue_stats.stats'] = array_merge(
        $base_plugin_definition,
        [
          'title' => 'Statistics',
          'route_name' => 'queue_stats.admin_form',
          'base_route' => 'queue_ui.overview_form',
        ]
      );
    }
    return $this->derivatives;
  }

}
