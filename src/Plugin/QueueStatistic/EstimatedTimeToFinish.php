<?php

namespace Drupal\queue_stats\Plugin\QueueStatistic;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\queue_stats\MonitoredQueueInterface;
use Drupal\queue_stats\Plugin\DateIntervalStatistic;
use Drupal\queue_stats\Plugin\QueueStatisticBase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to calculate how long time it will take to process all current items.
 *
 * @QueueStatistic(
 *   id = "time_to_finish",
 *   label = "Estimated time to finish",
 *   order = 1
 * )
 */
class EstimatedTimeToFinish extends QueueStatisticBase implements ContainerFactoryPluginInterface {
  use ContainerAwareTrait, DateIntervalStatistic;

  /**
   * EstimatedTimeToFinish constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The dependency injection container.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    ContainerInterface $container,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->container = $container;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container,
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(MonitoredQueueInterface $queue) {
    /* @var \Drupal\queue_stats\Plugin\QueueStatisticManager $queue_stat_manager */
    $queue_stat_manager = $this->container->get('plugin.manager.queue_stat');
    $processing_rate = $queue_stat_manager->createInstance('processing_rate');

    $processing_rate = $processing_rate->getValue($queue);
    if ($processing_rate) {
      return $queue->numberOfItems() / $processing_rate;
    }
  }

}
