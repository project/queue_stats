<?php

namespace Drupal\queue_stats\Plugin\QueueStatistic;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\queue_stats\Event\QueueItemEvent;
use Drupal\queue_stats\MonitoredQueueInterface;
use Drupal\queue_stats\Plugin\DateIntervalStatistic;
use Drupal\queue_stats\Plugin\QueueStatisticBase;
use Drupal\queue_stats\Plugin\StatefulStatistic;
use Drupal\queue_stats\Statistics\ExponentialMovingAverage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Determine how long a time spends in the queue before being processed.
 *
 * @QueueStatistic(
 *   id = "queue_time",
 *   label = "Time spent in queue"
 * )
 */
class QueueTime extends QueueStatisticBase implements EventSubscriberInterface, ContainerFactoryPluginInterface {
  use StatefulStatistic, DateIntervalStatistic;

  /**
   * QueueTime constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\State\StateInterface $state
   *   The site state.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    StateInterface $state,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->state = $state;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('state'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      QueueItemEvent::PROCESSING_COMPLETED => 'trackCompletion'
    ];
  }

  /**
   * Event handler for tracking when items have finished processing.
   *
   * @param \Drupal\queue_stats\Event\QueueItemEvent $event
   *   The event.
   */
  public function trackCompletion(QueueItemEvent $event) {
    $value = $this->retrieveValue($event->getQueue(), 'value', new ExponentialMovingAverage(10));
    $value->add($event->getTimestamp() - $event->getItem()->created);
    $this->storeValue($event->getQueue(), 'value', $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(MonitoredQueueInterface $queue) {
    /** @var \Drupal\queue_stats\Statistics\ExponentialMovingAverage $average */
    $average = $this->retrieveValue($queue, 'value', new ExponentialMovingAverage(10));
    return $average->average();
  }

  /**
   * {@inheritdoc}
   */
  public function reset(MonitoredQueueInterface $queue) {
    $this->deleteValue($queue, 'value');
  }

}
