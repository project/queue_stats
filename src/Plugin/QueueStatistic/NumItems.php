<?php

namespace Drupal\queue_stats\Plugin\QueueStatistic;

use Drupal\queue_stats\Annotation\QueueStatistic;
use Drupal\queue_stats\MonitoredQueueInterface;
use Drupal\queue_stats\Plugin\QueueStatisticBase;

/**
 * Determine how many items are in a queue currently.
 *
 * @QueueStatistic(
 *   id = "num_items",
 *   label = "Number of items",
 *   order = -100
 * )
 */
class NumItems extends QueueStatisticBase {

  /**
   * {@inheritdoc}
   */
  public function getValue(MonitoredQueueInterface $queue) {
    return $queue->numberOfItems();
  }

}
