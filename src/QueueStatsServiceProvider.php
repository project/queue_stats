<?php

namespace Drupal\queue_stats;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Queue statistics service provider.
 */
class QueueStatsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Use our modified Queue UI plugin manager. We have to do it this way as
    // we cannot override services by just reusing the service name in
    // queue_stats.services.yml.
    try {
      $definition = $container->getDefinition('plugin.manager.queue_ui');
      $definition->setClass(MonitoredQueueUIManager::class);
    }
    catch (ServiceNotFoundException $e) {
      // Do nothing. Queue UI is not required.
    }
  }

}
