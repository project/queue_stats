<?php

namespace Drupal\queue_stats;

use Drupal\queue_ui\QueueUIManager;

/**
 * Specialized Queue UI manager for plugins which handles monitored queues.
 *
 * Monitored queues essentially decorates other queue implementations and thus
 * have no idea how to display their content. This manager ensures that this
 * responsibility is passed back to the decorated queue.
 */
class MonitoredQueueUIManager extends QueueUIManager {

  /**
   * {@inheritdoc}
   */
  public function queueClassName($queue) {
    // The queue class name determines which plugin will be used to
    // handle Queue UI actions. Use the decorated queue when monitoring.
    if ($queue instanceof MonitoredQueue) {
      $monitored_queue = $queue->getMonitoredQueue();
      return parent::queueClassName($monitored_queue);
    }
    else {
      return parent::queueClassName($queue);
    }
  }

}
