<?php

namespace Drupal\queue_stats\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\queue_stats\MonitoredQueueInterface;

/**
 * Queue item event class.
 *
 * This will be dispatched at different stages of the item lifecycle.
 */
class QueueItemEvent extends Event {

  const PROCESSING_STARTED = 'queue_stats_item_processing_started';
  const PROCESSING_ABORTED = 'queue_stats_item_processing_stopped';
  const PROCESSING_COMPLETED = 'queue_stats_item_processing_completed';

  /**
   * The event name.
   *
   * @var string
   */
  protected $name;

  /**
   * The queue the item is/was related to.
   *
   * @var \Drupal\queue_stats\MonitoredQueueInterface
   */
  protected $queue;

  /**
   * The item which the event originated from.
   *
   * @var object
   */
  protected $item;

  /**
   * The timestamp when the event occured. This includes milliseconds.
   *
   * @var float
   */
  protected $timestamp;

  /**
   * QueueItemEvent constructor.
   *
   * @param string $name
   *   Event name.
   * @param \Drupal\queue_stats\MonitoredQueueInterface $queue
   *   Related queue.
   * @param object $item
   *   Queue item.
   * @param float $timestamp
   *   Timestamp including milliseconds.
   */
  public function __construct(string $name, MonitoredQueueInterface $queue, $item, float $timestamp) {
    $this->name = $name;
    $this->queue = $queue;
    $this->item = $item;
    $this->timestamp = $timestamp;
  }

  /**
   * Returns the name of the event.
   *
   * @return string
   *   Event name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Returns the related queue.
   *
   * @return \Drupal\queue_stats\MonitoredQueueInterface
   *   Related queue.
   */
  public function getQueue() {
    return $this->queue;
  }

  /**
   * Returns the queue item.
   *
   * @return object
   *   The queue item.
   */
  public function getItem() {
    return $this->item;
  }

  /**
   * The timestamp including milliseconds when the event occurred.
   *
   * @return float
   *   Event timestamp including milliseconds.
   */
  public function getTimestamp() {
    return $this->timestamp;
  }

}
