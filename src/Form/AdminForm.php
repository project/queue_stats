<?php

namespace Drupal\queue_stats\Form;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\queue_stats\Plugin\QueueStatisticManager;
use Drupal\queue_stats\QueueStatisticsCollectorFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue statistics administration form.
 */
class AdminForm extends FormBase {

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * The queue stat manager.
   *
   * @var \Drupal\queue_stats\Plugin\QueueStatisticManager
   */
  protected $queueStatManager;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * AdminForm constructor.
   *
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   The queue worker manager.
   * @param \Drupal\queue_stats\Plugin\QueueStatisticManager $queue_stat_manager
   *   The The queue stat manager.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   */
  public function __construct(
    QueueWorkerManagerInterface $queue_worker_manager,
    QueueStatisticManager $queue_stat_manager,
    BlockManagerInterface $block_manager
  ) {
    $this->queueWorkerManager = $queue_worker_manager;
    $this->queueStatManager = $queue_stat_manager;
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.queue_worker'),
      $container->get('plugin.manager.queue_stat'),
      $container->get('plugin.manager.block')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'queue_stats_admin';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build = [];

    /* @var \Drupal\Core\Block\BlockPluginInterface $block */
    $block = $this->blockManager->createInstance('queue_stats_list_block', []);
    $build['stats'] = [
      '#type' => 'details',
      '#title' => $this->t('All statistics'),
      '#open' => TRUE,
    ];
    $build['stats'][] = $block->build();

    $build['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset statistics')
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->queueStatManager->resetStatistics();
  }

}
