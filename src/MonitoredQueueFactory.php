<?php

namespace Drupal\queue_stats;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Queue factory decorator with support for monitoring queues.
 *
 * There is no queue factory interface so we have to extend the class instead.
 */
class MonitoredQueueFactory extends QueueFactory {

  /**
   * The decorated queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The event dispatcher for queue events.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * The current time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * MonitoredQueueFactory constructor.
   */
  public function __construct(QueueFactory $queue_factory, EventDispatcherInterface $dispatcher, TimeInterface $time) {
    // We are intentionally not calling the parent constructor. This is a
    // decorator.
    $this->queueFactory = $queue_factory;
    $this->dispatcher = $dispatcher;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function get($name, $reliable = FALSE) {
    $queue = $this->queueFactory->get($name, $reliable);
    return new MonitoredQueue($queue, $name, $this->dispatcher, $this->time);
  }

}
