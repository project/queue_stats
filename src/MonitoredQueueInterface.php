<?php

namespace Drupal\queue_stats;

use Drupal\Core\Queue\QueueGarbageCollectionInterface;
use Drupal\Core\Queue\QueueInterface;

/**
 * Interface for queues that are monitored by this module.
 */
interface MonitoredQueueInterface {

  /**
   * Returns the name of the queue.
   *
   * @return string
   *   Queue name.
   */
  public function getName();

  /**
   * Retrieves the number of items in the queue.
   *
   * @return int
   *   An integer estimate of the number of items in the queue.
   *
   * @see \Drupal\Core\Queue\QueueInterface::numberOfItems()
   */
  public function numberOfItems();

}
